---

title: Unbabel Translation in Zendesk
category: Zendesk
description: Implementation and use of Unbabel in Support's ZenDesk.
---



### Overview

This page has been moved. Please see
[Zendesk Global Apps - Unbabel](../support-ops/documentation/zendesk_global_apps.html#unbabel-for-zendesk-support)
for more information.
